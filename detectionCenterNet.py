#!/usr/bin/env python3
# -*- coding: utf-8 -*-


import sys

sys.path.append('./SiamMask/')
sys.path.append('./SiamMask/experiments/siammask_sharp/')
from SiamMask.experiments.siammask_sharp.custom import Custom
from SiamMask.utils.load_helper import load_pretrain
from SiamMask.tools.test import siamese_init, siamese_track

import CenterNet.src._init_paths
from CenterNet.src.lib.detectors.ctdet import CtdetDetector
from CenterNet.src.lib.opts import opts
from CenterNet.src.lib.utils_center_net.debugger import coco_class_name

import json
import cv2
import numpy as np
import time
import colorsys
import argparse
import os
import random

global lista, listaframes, listaclass, listalocation, listascore, aleatorio

lista=[]
listaframes=[]
listaclass=[]
listalocation=[]
listascore=[]

def click_event(event, x, y, flags, *param): 
	global clickx,clicky
	# checking for left mouse clicks 
	if event == cv2.EVENT_LBUTTONDOWN: 
  
        # displaying the coordinates 
        # on the Shell
		print(x,' ', y) 
		clickx=x
		clicky=y
		print(clickx, clicky)
		return clickx, clicky
     
    # checking for right mouse clicks      
	if event==cv2.EVENT_RBUTTONDOWN: 
  
        # displaying the coordinates 
        # on the Shell 
		print(x, ' ', y) 
		clikx=x
		cliky=y
  
		return clickx, clicky
       
def FindPoint(rec, x, y) : 
    if (x > rec[0] and x < rec[2] and 
        y > rec[1] and y < rec[3]) : 
        return True
    else : 
        return False

def  OverlapArea(rec1, rec2):
	area1=(rec1[2]-rec1[0])*(rec1[3]-rec1[1])
	area2=(rec2[2]-rec2[0])*(rec2[3]-rec2[1])
	dx=min(rec1[2], rec2[2]) - max(rec1[0], rec2[0])
	dy=min(rec1[3], rec2[3]) - max(rec1[1], rec2[1])
	if (dx >= 0) and (dy >= 0):
		areaover=dx*dy
		percent=areaover/(area1+area2-areaover)
		return percent
	else:
		return 0.0

class NumpyEncoder(json.JSONEncoder):
			def default(self, obj):
				if isinstance(obj, np.integer):
					return int(obj)
				elif isinstance(obj, np.floating):
					return float(obj)
				elif isinstance(obj, np.ndarray):
					return obj.tolist()
				return json.JSONEncoder.default(self, obj)


class Object:
	def __init__(self, num_frame, object_class, location, score):
		self.num_frame= num_frame
		self.object_class = object_class
		self.location = location
		self.score = score
		
def detection():

	with open('lista.txt', 'w') as file:
		for obj in lista:
			file.write("frame: ")
			file.write("%d\r\n" % obj.num_frame)	
			file.write("object: ")
			file.write("%s\r\n" % obj.object_class)
			file.write("location: ")
			dumped= json.dumps(obj.location, cls=NumpyEncoder)
			file.write(json.dumps(dumped))
			file.write("\nscore: ")
			file.write("%f\r\n" % obj.score)
	with open('listaprocess.txt', 'w') as file:
		for obj in lista:
			file.write("%d\r" % obj.num_frame)	
			file.write("%s\r" % obj.object_class)
			dumped= json.dumps(obj.location, cls=NumpyEncoder)
			file.write(json.dumps(dumped))
			file.write("")
			file.write("%f\r\n" % obj.score)
	#np.savetxt("listaframes.txt", listaframes, fmt="%d")
	#np.savetxt("listaclass.txt", listaclass, fmt="%s", delimiter=',')
	#np.savetxt("listalocation.txt", listalocation, fmt="%f")
	#np.savetxt("listascore.txt", listascore, fmt="%f")
	for obj in lista:
		print("frame:", obj.num_frame,"object:", obj.object_class,"location:", obj.location, "score:", obj.score)	
"""	lista_dict= {'lista_dict': lista_dict}




	thefile=open('lista.txt','w')

	for obj in lista: 
		thefile.write("\n" % obj.num_frame)
		thefile.write("\n" % obj.object_class)
	
	for Object in lista:
		if Object.num_frame==num_frame:
			print Object.object_class, Object.location, Object.score
"""
	

def get_colors(num_classes):
	hsv_tuples = [(x / num_classes, 1., 1.)
				  for x in range(num_classes)]
	colors = list(map(lambda x: colorsys.hsv_to_rgb(*x), hsv_tuples))
	colors = list(
		map(lambda x: (int(x[0] * 255), int(x[1] * 255), int(x[2] * 255)),
			colors))
	np.random.seed(10101)  # Fixed seed for consistent colors across runs.
	np.random.shuffle(colors)  # Shuffle colors to decorrelate adjacent classes.
	np.random.seed(None)  # Reset seed to default.
	
	return colors

def print_box(img, results, class_names, score_thr, colors, num_frame):
	num_classes = len(class_names)
	for num_class in range(1, num_classes+1):
		for bbox in results[num_class]:
			if bbox[4] > score_thr:
			 
				cat = int(num_class)-1
				conf = bbox[4]
				bbox = np.array(bbox[:4], dtype=np.int32)
				c = colors[cat]
				
				txt = '{}{:.2}'.format(coco_class_name[cat], conf)
				
				"""nombres=coco_class_name[cat]"""
				font = cv2.FONT_HERSHEY_SIMPLEX
				cat_size = cv2.getTextSize(txt, font, 0.5, 2)[0]
				cv2.rectangle(img, (bbox[0], bbox[1]), (bbox[2], bbox[3]), c, 2)
			 
				cv2.rectangle(img,
						(bbox[0], bbox[1] - cat_size[1] - 2),
						(bbox[0] + cat_size[0], bbox[1] - 2), c, -1)
				cv2.putText(img, txt, (bbox[0], bbox[1] - 2), 
					  font, 0.5, (0, 0, 0), thickness=1, lineType=cv2.LINE_AA)
				"""Object.object_class=coco_class_name[cat]"""
				"""print(Object.object_class)"""
	
				lista.append( Object(num_frame, coco_class_name[cat], [bbox[0], bbox[1], bbox[2], bbox[3]], conf))
				listaframes.append(num_frame)
				listaclass.append(coco_class_name[cat])
				listalocation.append([bbox[0], bbox[1], bbox[2], bbox[3]])
				listascore.append(conf)
				
				

def main():
	
	parser = argparse.ArgumentParser()
	parser.add_argument("video", help='input video')
	parser.add_argument("--weights_det", default='./CenterNet/models/ctdet_coco_dla_2x.pth', help='CenterNet weights')
	parser.add_argument("--weights_track", default='./SiamMask/experiments/siammask_sharp/SiamMask_DAVIS.pth', help='SiamMask weights')
	parser.add_argument("--gpu", default='0', help='gpu id to use')
	parser.add_argument("--skip_frames", default=1, type=int, help='process one frame for each skip_frames')
	parser.add_argument("--frame_size", default=608, type=int, help='size of the frame to show')
	parser.add_argument("--no_detect", action='store_true', help='True to perform Detection')
	parser.add_argument("--detect_thr", default=0.3, help='Detection threshold')
	parser.add_argument("--no_track", action='store_true', help='True to perform tracking')
	parser.add_argument("--track_thr", default=0.7, help='Tracking threshold')
	args = parser.parse_args()
	
	
	os.environ['CUDA_VISIBLE_DEVICES'] = str(args.gpu)
	
	# Initialize CenterNet
	cn_opts = opts().init(args=['ctdet', '--load_model', args.weights_det])
	detector = CtdetDetector(cn_opts)
	
	
	device = 'cuda' 				# if args.gpu != '' else 'cpu'
	cfg = json.load(open('./SiamMask/experiments/siammask_sharp/config_davis.json'))
	siammask = Custom(anchors=cfg['anchors'])
	siammask = load_pretrain(siammask, args.weights_track)
	siammask.eval().to(device)

	class_names = coco_class_name
	colors = get_colors(len(class_names))
	
	font = cv2.FONT_HERSHEY_SIMPLEX
	cam = cv2.VideoCapture(args.video)
	fps = cam.get(cv2.CAP_PROP_FPS)
	
	track_state = False
	
	detect = not args.no_detect; track = not args.no_track
	assert detect or track
	
	if detect: mode = 'detect'
	elif track: mode = 'track'
	
	else: raise ValueError('No mode selected')

	num_frame=0
	ti = time.time()
	aleatorio=random.randint(0,100)
	randomstr=str(aleatorio)
	while True:
		
		b, frame = cam.read()
		cv2.namedWindow('input')
		if not b: break
		"""Object.num_frame=num_frame"""
		
		"""print(Object.num_frame)"""
		
		print(num_frame)

		if num_frame % args.skip_frames != 0: 
			continue
	
		frame = cv2.resize(frame, (args.frame_size,frame.shape[0]*args.frame_size//frame.shape[1]))
	
	
		if detect and mode == 'detect':
			# Run detection
			ret = detector.run(frame)
			print_box(frame, ret['results'], class_names, args.detect_thr, colors, num_frame)
			#Save ret in json
		
			
			dumped= json.dumps(ret, cls=NumpyEncoder)
			with open('ret.txt', 'w') as f:
				json.dump(dumped, f)
			track_state = False
			
		if track and mode == 'track':
			# Run tracking 
			if track_state != False:
				track_state_new = siamese_track(track_state, frame, mask_enable=True, refine_enable=True, device=device)  # track
				"""Object.score = track_state['score']"""
				"""print(Object.score)"""
				if track_state['score'] > args.track_thr:
					# Update and show tracking state if it's above threshold
					track_state = track_state_new
					location = track_state['ploygon'].flatten()
					lista.append( Object(num_frame, randomstr, location, track_state['score']))
					#listaframes.append(num_frame)
					#listaclass.append(randomstr)
					#location1=np.array(location)
					#listalocation.append(location)
					#listascore.append(track_state['score'])
					"""print(Object.location)"""
					mask = track_state['mask'] > track_state['p'].seg_thr
				
					frame[:, :, 2] = (mask > 0) * 255 + (mask == 0) * frame[:, :, 2]
					cv2.polylines(frame, [np.int0(location).reshape((-1, 1, 2))], True, (0, 255, 0), 3)	
					
			
		key = cv2.waitKey(1)
		if key == ord('q'):
			# Exist video
			cv2.destroyAllWindows()
			break  # esc to quit
		elif track and key == ord('r'):
			# Select ROI
			if mode == 'track': 
				# Change mode to detect
				print('Detecting')
				mode = 'detect'
			else:
				# Change mode to track and init tracker
				detection()
				#print(lista[-1].location)
				
				cv2.setMouseCallback('input', click_event)
				cv2.waitKey(0)
				unoigual=False
				inside=False
				length=len(listaframes)
				print(num_frame)
				
				for x in range(0, length):					
					if ((num_frame)==listaframes[x]):
						
						print("ENTRA")
						inside=False
						inside= FindPoint(listalocation[x], clickx, clicky)
								#print(Overlap)
						print(listalocation[x])
							
						if (inside==True):
							x, y, x2, y2=listalocation[x]
							print(x, y, x2, y2)
							break
						#else:
						#	x,y,x2,y2=listalocation[x]
				print("sale del bucle")
				h=abs(y2-y)
				w=abs(x2-x)
				print(x, y, w, h)
				if x == y == w == h == 0: continue
				target_pos = np.array([x + w / 2, y + h / 2])
				target_sz = np.array([w, h])
				track_state = siamese_init(frame, target_pos, target_sz, siammask, cfg['hp'], device=device)
				print('Tracking')
				mode = 'track'
				#Reinicio lista
				#listaframes.clear()
				#listaclass.clear()
				#listalocation.clear()
				#listascore.clear()
				lista.clear()
		elif track and key == ord('t'):
			# Select ROI
			if mode == 'track': 
				# Change mode to detect
				print('Detecting')
				mode = 'detect'
			else:
				# Change mode to track and init tracker
				detection()
				#print(lista[-1].location)
				init_rect=cv2.selectROI('input', frame, False, False)
				print(init_rect)
				x3, y3, w, h = init_rect
				recROI=[x3, y3, x3+w, y3+h]
				unoigual=False
				Area=0.0
						
				cont=0
				#print(init_rect)
				length=len(listaframes)
				print(num_frame)
				for x in range(0, length):
					cont=cont+1
					print(cont)
					print(listaframes[x])
							
					if ((num_frame)==listaframes[x]):
								
						print("ENTRA")
							
						Area=OverlapArea(recROI, listalocation[x])
										#print(Overlap)
						print(listalocation[x])
						print(recROI)
						print(Area)	
						if (Area>=0.4):
									
							unoigual=True
							x, y, x2, y2=listalocation[x]
							print(x, y, x2, y2)
							break
				print("sale del bucle")
				h=abs(y2-y)
				w=abs(x2-x)
				print(x, y, w, h)
				if x == y == w == h == 0: continue
				target_pos = np.array([x + w / 2, y + h / 2])
				target_sz = np.array([w, h])
				track_state = siamese_init(frame, target_pos, target_sz, siammask, cfg['hp'], device=device)
				print('Tracking')
				mode = 'track'
				#Reinicio lista
				#listaframes.clear()
				#listaclass.clear()
				#listalocation.clear()
				#listascore.clear()
				lista.clear()
		elif track and key == ord('y'):
			# Select ROI
			if mode == 'track': 
				# Change mode to detect
				print('Detecting')
				mode = 'detect'
			else:
				# Change mode to track and init tracker
				init_rect = cv2.selectROI('input', frame, False, False)
				x, y, w, h = init_rect
				if x == y == w == h == 0: continue
				target_pos = np.array([x + w / 2, y + h / 2])
				target_sz = np.array([w, h])
				track_state = siamese_init(frame, target_pos, target_sz, siammask, cfg['hp'], device=device)
	
				print('Tracking')
				mode = 'track'
				#Reinicio lista
				#listaframes.clear()
				#listaclass.clear()
				#listalocation.clear()
				#listascore.clear()
				lista.clear()
				
	 	# Print FPS
		tf = time.time()
		t_frame = tf-ti

	 	# Wait until next frame time
		sleep_sec = (1/fps*args.skip_frames) - t_frame
		time.sleep(max(0, sleep_sec))
		ti = time.time()

		# Show FPSs
		txt = 'FPS: {:.1f}'.format((1/(sleep_sec+t_frame)*args.skip_frames))
		fps_size = cv2.getTextSize(txt, font, 0.5, 2)[0]
		cv2.rectangle(frame,
	 			(0, fps_size[1] + 4), (fps_size[0], 0), (255,0,0), -1)
		cv2.putText(frame, txt, (0, fps_size[1] + 2), font, 0.5, (0, 0, 0), thickness=1, lineType=cv2.LINE_AA)
	 	
	 	# Show frame and predictions
		cv2.imshow('input', frame)
		if num_frame == 0: cv2.waitKey(0)
		num_frame = num_frame +1 
	detection()
	cv2.destroyAllWindows()
	
if __name__ == '__main__': main()
