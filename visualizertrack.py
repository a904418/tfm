import sys
import json
import cv2
import numpy as np
from numpy import loadtxt
import colorsys
import os
import argparse
import time
import random
import re
import ast



		
class Object:
	def __init__(self, num_frame, object_class, location, score):			
		self.num_frame= num_frame
		self.object_class = object_class
		self.location = adapt_location(location)
		self.score = score
	
	def __repr__(self):
		return f"{self.object_class} with score {self.score} at frame {self.num_frame} and position {self.location}"


class Tracker:
	def __init__(self):	
		self.tracker = cv2.TrackerKCF_create()
		self.bounding_box = None
		self.ok = False
		self.name = None

	def initialize(self, frame_num, frame, bbox):
		self.bounding_box = bbox
		self.tracker.init(frame, self.bounding_box)
		self.name = str(random.randrange(0,100))
		
	
	def initialize_from_object(self, frame_num, frame, obj):
		self.bounding_box = (obj.location[0], obj.location[1], obj.location[2]- obj.location[0], obj.location[3]-obj.location[1])
		self.tracker.init(frame, self.bounding_box)
		self.name = obj.object_class + str(random.randrange(0,100))

	def update(self, frame_num, frame):
		if self.bounding_box is not None:
			# grab the new bounding box coordinates of the object
			(self.ok, bbox) = self.tracker.update(frame)
			if self.ok:
				self.bounding_box = bbox
			else:
				print("lost")
				self.bounding_box = None

	def get_object(self, frame_num):
		if self.bounding_box:
			return Object(frame_num, self.name, [self.bounding_box[0], self.bounding_box[1], self.bounding_box[0] + self.bounding_box[2], self.bounding_box[1] + self.bounding_box[3]], 1.0)
		
		return None

	def plot(self, frame):
		if self.ok and self.bounding_box is not None:
			(x, y, w, h) = [int(v) for v in self.bounding_box]
			cv2.rectangle(frame, (x, y), (x + w, y + h), (0, 255, 0), 2)


def adapt_location(location):
	if len(location) == 4:
		return location
	
	return [min(location[::2]), min(location[1::2]), max(location[::2]), max(location[1::2])]

colors = {}

def load_object_file(filename):
	with open(filename, 'r') as f:
		frame = None
		score = None
		location = None
		name = None
		objects = []
		for line in f:			
			frame_match = re.match("frame: (\d+)", line)
			object_match = re.match("object: (\w+)", line)
			location_match = re.match("location: \"(.+)\"", line)
			score_match = re.match("score: (.+)", line)
			if frame_match:
				frame = int(frame_match[1]) 
			if object_match:
				name = object_match[1]
			if location_match:
				location = ast.literal_eval(location_match[1])
			if score_match:
				score = float(score_match[1])
				
				objects.append(Object(frame, name, location, score))
		return objects
			

def get_objects(files):
	return sorted([item for filename in files for item in load_object_file(filename)], key=lambda o: o.num_frame)
	

def print_box(frame, num_frame, clase, location, score):
	x, y, x1, y1=location
	intx=int(x)
	inty=int(y)
	intx1=int(x1)
	inty1= int(y1)
	if clase not in colors:
		colors[clase] = (random.randrange(128, 256), random.randrange(128, 256), random.randrange(128, 256))
	color= colors[clase]
	thickness=1
	#print(num_frame)
	#print(x, y, x1, y1)
	
	txt = '{} {:.2}'.format(clase, score)
	font = cv2.FONT_HERSHEY_SIMPLEX
	cv2.rectangle(frame, (intx,inty),(intx1,inty1), color, thickness)
	cat_size = cv2.getTextSize(txt, font, 0.5, 2)[0]
	#cv2.rectangle(frame, (intx, inty - cat_size[1] - 2), (intx + cat_size[0], inty - 2), color, -1)
	#cv2.putText(frame, txt, (intx, inty - 2), font, 0.5, (0, 0, 0), thickness=1, lineType=cv2.LINE_AA)
	cv2.rectangle(frame, (intx, inty), (intx + cat_size[0], inty + cat_size[1]+4), color, -1)
	cv2.putText(frame, txt, (intx, inty+cat_size[1]+2), font, 0.5, (0, 0, 0), thickness=1, lineType=cv2.LINE_AA)
"""
def print_box(img, results, class_names, score_thr, colors, num_frame):
	num_classes = len(class_names)
	for num_class in range(1, num_classes+1):
		for bbox in results[num_class]:
			 
				cat = int(num_class)-1
				conf = bbox[4]
				bbox = np.array(bbox[:4], dtype=np.int32)
				c = colors[cat]
				
				txt = '{}{:.2}'.format(coco_class_name[cat], conf)
				
				
				font = cv2.FONT_HERSHEY_SIMPLEX
				cat_size = cv2.getTextSize(txt, font, 0.5, 2)[0]
				cv2.rectangle(img, (bbox[0], bbox[1]), (bbox[2], bbox[3]), c, 2)
			 
				cv2.rectangle(img,
						(bbox[0], bbox[1] - cat_size[1] - 2),
						(bbox[0] + cat_size[0], bbox[1] - 2), c, -1)
				cv2.putText(img, txt, (bbox[0], bbox[1] - 2), 
					  font, 0.5, (0, 0, 0), thickness=1, lineType=cv2.LINE_AA)
				
"""	

def overlaps(obj, bb):
	area_bb = bb[2]*bb[3]
	x_dist = max(0, min(obj.location[2], bb[0] + bb[2])  - max(obj.location[0], bb[0]))
	y_dist = max(0, min(obj.location[3], bb[1] + bb[3])  - max(obj.location[1], bb[1]))
 
	return x_dist*y_dist > 0.75*area_bb

def inside(obj, mouse):	
	if mouse is None:
		return False
	return obj.location[0] <= mouse[0] <= obj.location[2] and obj.location[1] <= mouse[1] <= obj.location[3]
	
lista = []

class NumpyEncoder(json.JSONEncoder):
			def default(self, obj):
				if isinstance(obj, np.integer):
					return int(obj)
				elif isinstance(obj, np.floating):
					return float(obj)
				elif isinstance(obj, np.ndarray):
					return obj.tolist()
				return json.JSONEncoder.default(self, obj)

def detection():
	global lista
	if lista:
		with open('object-' + lista[0].object_class + '.txt', 'w') as file:
			for obj in lista:
				file.write("frame: ")
				file.write("%d\r\n" % obj.num_frame)	
				file.write("object: ")
				file.write("%s\r\n" % obj.object_class)
				file.write("location: ")
				dumped= json.dumps(obj.location, cls=NumpyEncoder)
				file.write(json.dumps(dumped))
				file.write("\nscore: ")
				file.write("%f\r\n" % obj.score)


mouse = None

def mouse_callback(event, x, y, flags, param):
	# grab references to the global variables
	global mouse
	# if the left mouse button was clicked, record the starting
	# (x, y) coordinates and indicate that cropping is being
	# performed
	if event == cv2.EVENT_LBUTTONDOWN or event == cv2.EVENT_RBUTTONDOWN:
		mouse = (x, y)
			

def main():
	global lista
	global mouse
	#global listaframes, listaclass, listalocation, listascore
	#listaframes=[]
	#listaclass=[]
	#listalocation=[]
	#listascore=[]

	#listaframes=loadtxt("listaframes.txt", comments="#", delimiter=None, unpack=False, dtype=int)
	#listaclass=loadtxt("listaclass.txt", comments="#", delimiter=None, unpack=False, dtype=str)
	#listalocation=loadtxt("listalocation.txt", comments="#", delimiter=None, unpack=False, dtype=float)
	#listascore=loadtxt("listascore.txt", comments="#", delimiter=None, unpack=False)

	parser = argparse.ArgumentParser()
	parser.add_argument("video", help='Input videofile')
	parser.add_argument('--objects', type=str, nargs='*', help='One or more files with a list of objects detections')
	parser.add_argument("--skip_frames", default=1, type=int, help='process one frame for each skip_frames')
	parser.add_argument("--frame_size", default=608, type=int, help='size of the frame to show')
	parser.add_argument("--output", default="", type=str, help='Output file. Must be a .avi file.')

	args = parser.parse_args()
	
	if args.objects:
		objects = get_objects(args.objects)
	else:
		objects = []
	

	#font = cv2.FONT_HERSHEY_SIMPLEX

	cam = cv2.VideoCapture(args.video)
	fps = cam.get(cv2.CAP_PROP_FPS)
	width = 608
	height = 342

	writer = None
	if args.output and args.output.endswith(".avi"):
		writer = cv2.VideoWriter(args.output, cv2.VideoWriter_fourcc(*'DIVX'), fps, (width, height))
		print(f"Output recorded into {args.output}")
	else:
		print("Output not recorded")

	if(not cam.isOpened()):
		print("Error opening video file")
		return

	num_frame=0
	ti = time.time()
	
	current_list_index = 0
	N = len(objects)
	speed = 1
	tracker = Tracker()

	lista = []
	detection()
	cv2.namedWindow(args.video)
	cv2.setMouseCallback(args.video, mouse_callback)

	while (cam.isOpened()):
		ret, frame = cam.read()
		detection()
		frame = cv2.resize(frame, (width,height), cv2.INTER_AREA)
		objects_to_print = []

		if ret==True:
			tracker.update(num_frame, frame)
			
			if tracker.ok:
				
				tracked_object = tracker.get_object(num_frame)
				objects_to_print.append(tracked_object)
				lista.append(tracked_object)

			while current_list_index < N and num_frame > objects[current_list_index].num_frame:
				current_list_index += 1
			
			if current_list_index < N:
				while current_list_index < N and num_frame == objects[current_list_index].num_frame:
					objects_to_print.append(objects[current_list_index]) 
					current_list_index += 1
			
			for obj in objects_to_print:
				print_box(frame, obj.num_frame, obj.object_class, obj.location, obj.score)
			
		else:
			detection()
			print ("The end")
			break
			

		# Print FPS
		tf = time.time()
		t_frame = tf-ti

	 	# Wait until next frame time
		sleep_sec = (1/fps*args.skip_frames) - t_frame
		time.sleep(max(0, sleep_sec/speed))
		ti = time.time()

		# Show FPSs
		#txt = 'FPS: {:.1f}'.format((1/(sleep_sec+t_frame)*args.skip_frames))
		#fps_size = cv2.getTextSize(txt, font, 0.5, 2)[0]
		#cv2.rectangle(frame, (0, fps_size[1] + 4), (fps_size[0], 0), (255,0,0), -1)
		#cv2.putText(frame, txt, (0, fps_size[1] + 2), font, 0.5, (0, 0, 0), thickness=1, lineType=cv2.LINE_AA)
	 	
	 	# Show frame and predictions
		cv2.imshow(args.video, frame)		
		
		key = cv2.waitKey(0 if num_frame == 0 else 1)

		if key & 0xFF == ord('q'):			
			print("Quit")
			break

		if key & 0xFF == ord('f'):
			speed = speed*1.25
		
		if key & 0xFF == ord('s'):
			speed = speed/1.25

		if key & 0xFF == ord('r'):
			speed = 1

		if key & 0xFF == ord('y'):
			initBB = cv2.selectROI(args.video, frame, fromCenter=False, showCrosshair=False)
			tracker.initialize(num_frame, frame, initBB)
			lista = []

		if key & 0xFF == ord('t'):
			initBB = cv2.selectROI(args.video, frame, fromCenter=False, showCrosshair=False)
			for obj in objects_to_print:
				if overlaps(obj, initBB):					
					tracker.initialize_from_object(num_frame, frame, obj)
					lista = []
					break

		if key & 0xFF == ord('r'):
			cv2.waitKey(0)	
			for obj in objects_to_print:
				if inside(obj, mouse):					
					tracker.initialize_from_object(num_frame, frame, obj)
					lista = []
					break
			
		if writer:
			writer.write(frame)
		
		num_frame += 1 


	
	cam.release()

	if writer:
		writer.release()
		

	cv2.destroyAllWindows()
	

if __name__ == '__main__': main()
